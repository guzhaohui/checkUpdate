package com.gzh.appcheckupdate

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gzh.library.UpdateChecker

/**
 * @创建者 顾朝辉
 * @创建时间 2020/12/5 15:13
 * @描述
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //使用方法
        //UpdateChecker.checkForDialog(supportFragmentManager, true, "此处填写下载链接")
    }
}