package com.gzh.library

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.gzh.library.util.CfuConstant

/**
 * 自定义更新提示框
 */
class UpdateDialog : DialogFragment(), DownloadService.OnProgressChangeListener {

    private var handler: Handler? = null
    private lateinit var cfuTitleTextView: TextView
    private lateinit var cfuContentTextView: TextView
    private lateinit var cfuCancelTextView: TextView
    private lateinit var cfuDownloadTextView: TextView
    private lateinit var cfuProgressTextView: TextView
    private lateinit var cfuProgressBar: ProgressBar
    private lateinit var cfuButtonRelativeLayout: RelativeLayout
    private lateinit var cfuProgressBarRelativeLayout: RelativeLayout

    @SuppressLint("HandlerLeak")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        DownloadService.onProgressChangeListener = this
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = inflater.inflate(R.layout.cfu_fragment_custom_dialog, null)
        cfuTitleTextView = view.findViewById(R.id.cfuTitleTextView)
        cfuContentTextView = view.findViewById(R.id.cfuContentTextView)
        cfuCancelTextView = view.findViewById(R.id.cfuCancelTextView)
        cfuDownloadTextView = view.findViewById(R.id.cfuDownloadTextView)
        cfuProgressTextView = view.findViewById(R.id.cfuProgressTextView)
        cfuProgressBar = view.findViewById(R.id.cfuProgressBar)
        cfuButtonRelativeLayout = view.findViewById(R.id.cfuButtonRelativeLayout)
        cfuProgressBarRelativeLayout = view.findViewById(R.id.cfuProgressBarRelativeLayout)
        // 取消更新
        cfuCancelTextView.setOnClickListener {
            CfuConstant.IS_UPDATEDIALOG_SHOW = false
            dismiss()
        }
        // 立即下载
        cfuDownloadTextView.setOnClickListener {
            // 开启下载service
            goToDownload()
            cfuTitleTextView.text = getString(R.string.cfu_downloading)
            cfuButtonRelativeLayout.visibility = View.GONE
            cfuProgressBarRelativeLayout.visibility = View.VISIBLE
        }
        handler = object : Handler() {
            override fun handleMessage(msg: Message) {
                if (msg.what == 0) {
                    cfuProgressBar.progress = msg.arg1
                    cfuProgressTextView.text = "${msg.arg1}%"
                }
            }
        }
        CfuConstant.IS_UPDATEDIALOG_SHOW = true
        dialog?.setCancelable(true)
        dialog?.setCanceledOnTouchOutside(true)
        return view
    }

    private fun goToDownload() {
        val intent = Intent(activity?.applicationContext, DownloadService::class.java)
        intent.putExtra(CfuConstant.APK_DOWNLOAD_URL, arguments?.getString(CfuConstant.APK_DOWNLOAD_URL))
        if (Build.VERSION.SDK_INT >= 26) {
            activity?.startForegroundService(intent)
        } else {
            activity?.startService(intent)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        CfuConstant.IS_UPDATEDIALOG_SHOW = false
    }

    override fun onProgressChange(progress: Int) {
        val msg = Message.obtain()
        msg.what = 0
        msg.arg1 = progress
        handler?.sendMessage(msg)
    }
}