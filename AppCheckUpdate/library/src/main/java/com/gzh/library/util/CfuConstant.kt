package com.gzh.library.util

/**
 * @创建者 顾朝辉
 * @创建时间 2020/4/27 8:40
 * @描述
 */
class CfuConstant {
    companion object {
        //是否需要显示更新提示框
        var IS_UPDATEDIALOG_SHOW: Boolean = false
        const val APK_DOWNLOAD_URL = "path"
    }
}