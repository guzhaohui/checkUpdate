package com.gzh.library.util

import android.annotation.TargetApi
import android.app.ActivityManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.FileProvider
import java.io.File
import java.util.*


/**
 * @创建者 顾朝辉
 * @创建时间 2020/4/26 17:43
 * @描述 公共工具类
 */
object CfuPublicUtil {
    /**
     * 判断下载服务是否已开启
     *
     * @return
     */
    @Suppress("DEPRECATION")
    fun isServiceRunning(context: Context, serviceName: String?): Boolean {
        if ("" == serviceName || serviceName == null) return false
        val myManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningService: ArrayList<ActivityManager.RunningServiceInfo> =
            myManager.getRunningServices(Int.MAX_VALUE) as ArrayList<ActivityManager.RunningServiceInfo>
        for (i in 0 until runningService.size) {
            Log.e("运行中的service>>>>>>", runningService[i].service.className)
            if (runningService[i].service.className == serviceName) {
                return true
            }
        }
        return false
    }

    /**
     * 解决buildsdk >= 24,调用Uri.fromFile时报错
     *
     * @param context
     * @param file
     * @return
     */
    fun getFileUri(context: Context, file: File): Uri {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            FileProvider.getUriForFile(
                context.applicationContext,
                context.applicationContext.packageName + ".fileprovider",
                file
            )
        } else {
            Uri.fromFile(file)
        }
    }

    /**
     * 8.0通知渠道
     * 创建下载通知渠道
     * @param channelId 渠道id
     * @param channelName 渠道名
     * @param channelDescription 渠道描述
     * @param importance 重要程度
     *
     */
    @TargetApi(Build.VERSION_CODES.O)
    fun createNotificationChannel(
        context: Context?,
        channelId: String,
        channelName: String,
        channelDescription: String,
        importance: Int
    ) {
        val channel = NotificationChannel(channelId, channelName, importance)
        //角标
        channel.setShowBadge(false)
        //开启指示灯
        channel.enableLights(false)
        //设置锁屏展示
        channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
        //设置渠道描述
        channel.description = channelDescription
        //声音
        channel.setSound(null, null)
        val notificationManager = context?.getSystemService(
            Context.NOTIFICATION_SERVICE
        ) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    /**
     * 判断通知栏图标是否存在
     */
    fun isNotifyIconExsit(context: Context): Int {
        val idDrawable: Int = context.resources.getIdentifier(
            context.packageName + ":drawable/ic_notify_download",
            null, null
        )
        val idMipmap: Int = context.resources.getIdentifier(
            context.packageName + ":mipmap/ic_notify_download",
            null, null
        )
        return if (idDrawable != 0) idDrawable else idMipmap
    }
}