# checkUpdate

#### 介绍
    检测app更新

#### 使用说明
1.  
    
```
allprojects {
        repositories {
            ...
            maven { url 'https://jitpack.io' }
        }
    }
```

2. 
    `implementation 'com.gitee.guzhaohui:checkUpdate:1.0.0'`
3.
    使用接口获取到最新的版本号，与APP当前版本进行对比，如果有新的版本则执行如下代码：

    path即下载地址url（http://www.xxxxxxx/xxx.apk）

   
```
 if (!CfuConstant.IS_UPDATEDIALOG_SHOW) {
        if (path.isNotEmpty()) {
            UpdateChecker.checkForDialog(supportFragmentManager, true, path)
        }
    }
```

